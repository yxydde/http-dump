FROM golang:1.19.0 AS builder

WORKDIR /app

COPY main.go .

COPY go.mod .

RUN go build -a -o http-dump .

FROM debian:stable-slim

WORKDIR /app/

COPY --from=0 /app/http-dump .

EXPOSE 8080

CMD ["./http-dump"]
